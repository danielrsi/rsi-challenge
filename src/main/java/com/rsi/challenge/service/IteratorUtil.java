package com.rsi.challenge.service;

import java.util.*;

public class IteratorUtil {

    public static <T> Optional<T> removeAndReturnFirst(Collection<? extends T> c) {
        Iterator<? extends T> it = c.iterator();
        if (!it.hasNext()) {
            return Optional.empty();
        }
        T removed = it.next();
        it.remove();
        return Optional.ofNullable(removed);
    }

    public static <T> Set<T> removedAndReturnTopItems(Collection<? extends T> c, int count) {
        Iterator<? extends T> it = c.iterator();

        Set<T> result = new HashSet<>();

        while (it.hasNext() && result.size() < count) {
            result.add(it.next());
            it.remove();
        }

        return result;
    }
}
