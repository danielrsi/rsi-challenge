package com.rsi.challenge.service;

import com.rsi.challenge.domain.Document;
import com.rsi.challenge.domain.SLATier;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;

@Service
public class OrchestratorService {

    private final StateService stateService;

    public OrchestratorService(StateService stateService) {
        this.stateService = stateService;
    }

    public void registerFiles(Set<String> documentBatchDtoSet, String userId) {
        SLATier slaTier;

        // just mocking the tier based on the userId
        if (userId.startsWith("1")) {
            slaTier = SLATier.TIER_1;
        } else if (userId.startsWith("2")) {
            slaTier = SLATier.TIER_2;
        } else {
            slaTier = SLATier.TIER_3;
        }

        documentBatchDtoSet.forEach(fileName -> {
            Document document = new Document(UUID.randomUUID().toString(), fileName, userId);
            stateService.addDocumentToQueue(document, slaTier);
        });
    }

}
