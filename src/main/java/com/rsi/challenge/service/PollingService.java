package com.rsi.challenge.service;

import com.rsi.challenge.domain.Document;
import com.rsi.challenge.domain.SLATier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class PollingService {

    private final StateService stateService;
    private final ProcessorService processorService;
    private final ServerManagementService serverManagementService;

    public PollingService(StateService stateService, ProcessorService processorService, ServerManagementService serverManagementService) {
        this.stateService = stateService;
        this.processorService = processorService;
        this.serverManagementService = serverManagementService;
    }

    @Scheduled(fixedDelay = 1000)
    public void pollTier1() {

        Set<Document> documentSet = stateService.getTopDocumentsTier1();

        if (documentSet.isEmpty()) {
            return;
        }

        Optional<MockServer> optionalMockServer = serverManagementService.getTier1Servers()
                .stream()
                .filter(mockServer1 -> !mockServer1.isDone())
                .filter(mockServer1 -> mockServer1.roomLeft() >= documentSet.size())
                .findFirst();

        if (optionalMockServer.isPresent()) {
            optionalMockServer.get().addDocuments(documentSet);
        } else {
            MockServer mockServer = new MockServer(documentSet, processorService, SLATier.TIER_1);
            serverManagementService.runServer(mockServer);
        }
    }

    @Scheduled(fixedDelay = 1000)
    public void pollTier2() {

        Set<Document> documentSet = stateService.getTopDocumentsTier2();

        if (documentSet.isEmpty()) {
            return;
        }

        Optional<MockServer> optionalMockServer = serverManagementService.getTier2Servers()
                .stream()
                .filter(mockServer1 -> !mockServer1.isDone())
                .filter(mockServer1 -> mockServer1.roomLeft() >= documentSet.size())
                .findFirst();

        if (optionalMockServer.isPresent()) {
            optionalMockServer.get().addDocuments(documentSet);
        } else {
            MockServer mockServer = new MockServer(documentSet, processorService, SLATier.TIER_2);
            serverManagementService.runServer(mockServer);
        }
    }

    @Scheduled(fixedDelay = 1000)
    public void pollTier3() {
        // the 3rd tier wont create new servers, it will just process the top document at each iteration
        stateService.getDocumentTier3()
                .ifPresent(processorService::processDocument);
    }

    /**
     * As the servers are not real servers with an exit, this routine will cleanup the done servers
     */
    @Scheduled(fixedDelay = 5000)
    public void serversCleanup() {
        serverManagementService.cleanDoneServers();
    }

    /**
     * To help debugging
     */
    @Scheduled(fixedDelay = 7500)
    public void serverMonitor() {
        System.out.println("ACTIVE TIER 1 SERVICES: " + serverManagementService.getTier1Servers().size());
        System.out.println("ACTIVE TIER 2 SERVICES: " + serverManagementService.getTier2Servers().size());
    }

}
