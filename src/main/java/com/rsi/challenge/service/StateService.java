package com.rsi.challenge.service;

import com.rsi.challenge.domain.Document;
import com.rsi.challenge.domain.SLATier;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class StateService {

    private Set<Document> queueTier1 = new LinkedHashSet<>();
    private Set<Document> queueTier2 = new LinkedHashSet<>();
    private Set<Document> queueTier3 = new LinkedHashSet<>();

    private Set<Document> processedDocuments = new LinkedHashSet<>();

    public synchronized void addDocumentToQueue(Document document, SLATier slaTier) {
        switch (slaTier) {
            case TIER_1:
                queueTier1.add(document);
                break;
            case TIER_2:
                queueTier2.add(document);
                break;
            case TIER_3:
                queueTier3.add(document);
                break;
        }
    }

    public synchronized Set<Document> getTopDocumentsTier1(){
        return IteratorUtil.removedAndReturnTopItems(queueTier1, SLATier.TIER_1.getMaxDocumentsPerServer());
    }

    public synchronized Set<Document> getTopDocumentsTier2(){
        return IteratorUtil.removedAndReturnTopItems(queueTier2, SLATier.TIER_2.getMaxDocumentsPerServer());
    }

    public synchronized Optional<Document> getDocumentTier3() {
        return IteratorUtil.removeAndReturnFirst(queueTier3);
    }

    public synchronized void addProcessedDocument(Document document) {
        processedDocuments.add(document);
    }

    public Set<Document> getProcessedDocuments() {
        return processedDocuments;
    }
}
