package com.rsi.challenge.service;

import com.rsi.challenge.domain.Document;
import com.rsi.challenge.domain.SLATier;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class MockServer {

    private final String serverId;
    private final ProcessorService processorService;
    private final Set<Document> documents;
    private final SLATier slaTier;

    private volatile boolean isDone;

    public MockServer(Set<Document> documents, ProcessorService processorService, SLATier slaTier) {
        this.documents = documents;
        this.serverId = UUID.randomUUID().toString();
        this.processorService = processorService;
        this.slaTier = slaTier;
    }

    public void run() {
        while (documents.size() > 0) {
            IteratorUtil.removeAndReturnFirst(documents)
                    .ifPresent(processorService::processDocument);
        }

        // if this was an executable, here it would just exit
        this.isDone = true;
    }

    /**
     * @return the room left of documents that can be added to this server and still meet the SLA
     */
    public int roomLeft() {
        return slaTier.getMaxDocumentsPerServer() - documents.size();
    }

    public void addDocuments(Set<Document> documents) {
        this.documents.addAll(documents);
    }

    public boolean isDone() {
        return isDone;
    }

    public SLATier getSlaTier() {
        return slaTier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MockServer that = (MockServer) o;
        return Objects.equals(serverId, that.serverId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serverId);
    }

}
