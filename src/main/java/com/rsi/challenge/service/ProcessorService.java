package com.rsi.challenge.service;

import com.rsi.challenge.domain.Document;
import org.springframework.stereotype.Service;

@Service
public class ProcessorService {

    private final StateService stateService;

    public ProcessorService(StateService stateService) {
        this.stateService = stateService;
    }

    public void processDocument(Document document) {
        document.markStarted();
        try {
            // sleep 1 min
            Thread.sleep(60 * 1000L);
        } catch (InterruptedException e) {
            // not covering edge cases on this challenge
            throw new RuntimeException("Unexpected interrupt", e);
        }
        document.markCompleted();
        stateService.addProcessedDocument(document);
    }

}
