package com.rsi.challenge.service;

import com.rsi.challenge.domain.SLATier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServerManagementService {

    private Set<MockServer> tier1Servers;
    private Set<MockServer> tier2Servers;

    public ServerManagementService() {
        this.tier1Servers = new HashSet<>();
        this.tier2Servers = ConcurrentHashMap.newKeySet();
    }

    @Async
    public void runServer(MockServer mockServer) {
        if (mockServer.getSlaTier().equals(SLATier.TIER_1)) {
            this.tier1Servers.add(mockServer);
        } else {
            this.tier2Servers.add(mockServer);
        }

        mockServer.run();
    }

    public synchronized Set<MockServer> getTier1Servers() {
        return tier1Servers;
    }

    public synchronized Set<MockServer> getTier2Servers() {
        return tier2Servers;
    }


    public synchronized void cleanDoneServers() {
        tier1Servers.removeIf(MockServer::isDone);
        tier2Servers.removeIf(MockServer::isDone);
    }
}
