package com.rsi.challenge.domain;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

import java.util.Objects;

public class Document {

    public enum DocumentState {
        UPLOADED,
        PROCESSING,
        COMPLETED
    }

    private String docId;
    private String fileName;
    private String userId;
    private DocumentState documentState;
    private DateTime dtCreated;
    private DateTime dtStarted;
    private DateTime dtCompleted;


    public Document(String docId, String fileName, String userId) {
        this.docId = docId;
        this.fileName = fileName;
        this.userId = userId;
        this.documentState = DocumentState.UPLOADED;
        this.dtCreated = new DateTime();
    }

    public void markStarted() {
        this.dtStarted = new DateTime();
        this.documentState = DocumentState.PROCESSING;
    }

    public void markCompleted() {
        this.dtCompleted = new DateTime();
        this.documentState = DocumentState.COMPLETED;
    }

    public String getDocId() {
        return docId;
    }

    public String getFileName() {
        return fileName;
    }


    public String getUserId() {
        return userId;
    }

    public DocumentState getDocumentState() {
        return documentState;
    }

    public Integer getMinutesTaken() {
        if (this.dtCompleted != null) {
            return Minutes.minutesBetween(this.dtCreated, this.dtCompleted).getMinutes();
        }

        return null;
    }

    public DateTime getDtCreated() {
        return dtCreated;
    }

    public DateTime getDtStarted() {
        return dtStarted;
    }

    public DateTime getDtCompleted() {
        return dtCompleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return docId.equals(document.docId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(docId);
    }
}
