package com.rsi.challenge.domain;

public enum SLATier {
    /**
     * 10 minutes SLA
     */
    TIER_1(10),
    /**
     * 1 hour SLA
     */
    TIER_2(60),
    /**
     * best effort SLA
     */
    TIER_3(null);


    /**
     * The max amount of documents that can be processed in a server
     * Assuming a document takes 1 minute to process
     * Ex: Tier 1 can have up to 10 documents
     */
    private Integer maxDocumentsPerServer;

    public Integer getMaxDocumentsPerServer() {
        return maxDocumentsPerServer;
    }

    SLATier(Integer maxDocumentsPerServer) {
        this.maxDocumentsPerServer = maxDocumentsPerServer;
    }
}
