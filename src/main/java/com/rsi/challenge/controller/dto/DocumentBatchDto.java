package com.rsi.challenge.controller.dto;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

public class DocumentBatchDto {

    public DocumentBatchDto(@NotEmpty Set<String> files, @NotEmpty String userId) {
        this.files = files;
        this.userId = userId;
    }

    @NotEmpty
    private Set<String> files;

    @NotEmpty
    private String userId;

    public Set<String> getFiles() {
        return files;
    }

    public void setFiles(Set<String> files) {
        this.files = files;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
