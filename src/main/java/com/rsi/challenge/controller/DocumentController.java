package com.rsi.challenge.controller;

import com.rsi.challenge.controller.dto.DocumentBatchDto;
import com.rsi.challenge.service.OrchestratorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class DocumentController {

    private final OrchestratorService orchestratorService;

    public DocumentController(OrchestratorService orchestratorService) {
        this.orchestratorService = orchestratorService;
    }

    @PostMapping("/api/documents/submit")
    public ResponseEntity<Void> processDocuments(@Valid @RequestBody DocumentBatchDto documentBatchDto){
        orchestratorService.registerFiles(documentBatchDto.getFiles(), documentBatchDto.getUserId());
        return ResponseEntity.ok().build();
    }
}
