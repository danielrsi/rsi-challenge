package com.rsi.challenge;

import com.rsi.challenge.service.OrchestratorService;
import com.rsi.challenge.service.StateService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@SpringBootTest
class OrchestratorServiceIT {

    @Autowired
    private OrchestratorService orchestratorService;

    @Autowired
    private StateService stateService;

    @Test
    void testSla10Min() throws InterruptedException {

        // It's limiting to 10 threads, so 100 is the max amount that can be processed.
        int FILE_COUNT = 100;
        Set<String> fileNames = new HashSet<>();

        for (int i = 0; i < FILE_COUNT; i++) {
            fileNames.add(UUID.randomUUID().toString());
        }

        orchestratorService.registerFiles(fileNames, "1");

        // wait 11 min to verify
        Thread.sleep(11 * 60 * 1000);

        Assert.assertEquals(FILE_COUNT, stateService.getProcessedDocuments().size());
        stateService.getProcessedDocuments().forEach(document -> Assert.assertTrue(document.getMinutesTaken().toString(), document.getMinutesTaken() < 11));
    }


}
