# RSI Code challenge

## Considerations

* The servers are implemented as threads, therefore there is a limitation on how many files can be processed to meet the SLA, as there is a limitation on how many threads you can have. The intention was to just show the algorithm of how that would be done if the servers were really services.

* There are no real users, the SLA is hardcoded based on the `userId`. "1" for Tier-1, "2" for Tier-2 and "3" for Tier-3.

* To test the application you can call this API

````
curl --location --request POST 'http://localhost:8080/api/documents/submit' \
--header 'Content-Type: application/json' \
--data-raw '{
    "userId": "1",
    "files": ["test1", "test2", "test3", "test4"]
}'
````
* The thread safety of `ServerManagementService` was not fully implemented.
* Unit tests were not done as time was extrapolated.

## How it was designed

* The `StateService` mocks what would be a database. The documents uploaded are inserted there by the `OrchestratorService` with `DocumentState.UPLOADED` state.
* The `PollingService` have 3 scheduled jobs running every second and querying for documents to process in each of the SLAs.
* The `MockServer` mocks what would be an external server being created and destroyed dynamically as needed.
* The `ProcessorService` process the document, faking the duration of the process, hardcoded to 1 minute.
* The `PollingService` will get and remove the first X items of the queue, X being the maximum amount of items that can be processed while respecting the SLA.
    - For example: Given that a document takes 1 minute and the SLA is 10 minutes. It can get up to 10 documents from the top of the queue and starts a new server.